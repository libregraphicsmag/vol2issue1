A journey through form fields
Eric Schrijver

The screen you get confronted with when you log into a wordpress admin and you choose ‘new entry’, is the base of the online writing experience. Little has changed from the very first content management systems, where a single unstyled text field would let you pour in your thoughts and send them off. The only notable difference is that the interface now allows you to insert links and style your text. But this is not "what you see is what you get." There is still little resemblance between the field you fill in here, and the finished post that comes out at the other end.

//All together now//
The traditional web form field is a lonely place. You are typing in your information in the browser, and only when you hit a submit button the information is sent to the server. This means if you started editing a resource at the same time as someone else, the person who sends their information latest will override the contributions of the predecessor. Content management systems put in place editing locks, allowing only one user to edit at a time. Wikipedia employs a sophisticated merging tool to merge various edits together.

When I started working with OSP and Constant in 2010, I met Etherpad. Etherpad presents you with an online document allowing you to start typing. As you do, you might see others who connect start typing as well. There is no submit button. Everything is saved while you type so that it can be updated to the others at the same time. You are no longer solitary with your textbox.

Once you have used Etherpad to write, it becomes difficult to imagine writing collaboratively without it. In a collaborative book sprint in Rotterdam, we used booki.cc, which allows for sophistated PDF and ebook creation. Yet like many content editing tools it poses a single user content lock per chapter. At the end of the session it turned out everybody has used Etherpad to write their chapters together, before submitting it to the booki platform.

Will we see more etherpad-style collaboration online? Etherpad was bought by Google. Parts of it have been re-used in Google Docs and Google Wave, Google’s mixup of chat, wiki, blog and mail. The technology behind it has remained notoriously difficult to implement though. It is hard because it requires an intimate collaboration between what is traditionally considered the front- and the backend of the website, the site displaying the data and the system processing and storing it. You need a kind of persistent connection not offered by traditional web servers.

This technology is getting more accessible though. JavaScript, the language once used solely for creating scrolling ticker tape effects on web pages, is making its way into the backend, running in browsers and on servers alike. Frameworks like Meteor might make this technology accessible to a large number of developers in the same way that a framework like jQuery has enabled many designers to implement JavaScript effects and interactivity on their sites.

//The future is being written//
Open source writing tools have been hugely succesful. Wordpress powers 16% of the web. Wikipedia is the go-to resource for factual knowledge. As the intimate link between Wikipedia and its editing software shows, we can’t talk about the writing technologies of the future without talking about the texts of the future. Writing tools rise in response to, and at the same time give shape to, what is being written.
 
We immediately recognize the tone of Wikipedia even if an article is written by thousands of people. The tool, and the process it entails, stimulate a certain kind of discourse. At the same time, these tools have responded to needs and desires that where there already — the reason they were invented in the first place. Before one invents a writing tool, one might need to ask: ‘what do I want to write?’


