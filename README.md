# Localisation/Internationalization, Issue 2.1

## Index

* **Localisation and internationalization**, ginger coon — *Editor's letter*
* **Pruning branches**, Manufactura Independente — *Production colophon*
* **Mozilla Festival 2012** — *Notebook*
* *New Releases*
* **The transnational glitch**, Antonio Roberts — *Column*
* **A journey through form fields**, Eric Schrijver — *Column*
* **Sketching creative code**, Davide della Casa — *Dispatch*
* *Small & Useful*
* **Crafting Type: type design workshops around the world**, Dave Crossland — *Dispatch*
* **Styling maps like the web, for the web**, Pierros Papadeas — *First time*
* **Speaking across borders**, ginger coons — *Picture feature*
* **3 bridges**, Nikki Pugh — *Showcase*
* **De Schaarbeekse taal**, Clémentine Delahaut, Peter Westenberg, An Mertens (Constant) — *Showcase*
* **Newstweek**, Julian Oliver and Danja Vasiliev — *Showcase*
* **Hacking clothing: an interview with Susan Spencer**, Susan Spencer interviewed by Natalie Maciw — *Interview*
* **Localizing type**, Denis Jacquerye — *Feature*
* *Resources & Glossary*
* *Call for submissions: Gendering F/LOSS, Issue 2.2*

## Colophon

Localisation/Internationalization — February 2013
Issue 2.1, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416
