Crafting Type: Type Design Workshops, Around the World

Love letters? Many designers, artists and writers do. But how to craft typefaces has been shrouded in mystery. The Crafting Type project started with the motivation of changing this, by holding workshops which push participants through the entire type design process: from the secrets of sketching to digital drawing techniques to OpenType features, using only libre software -- mainly FontForge, plus some Inkscape and Scribus.

The workshops are held at design schools with attendance from both students and local professionals. Despite the variety of backgrounds in design, since all participants are beginners in type design, the mix of students and professionals adds a real buzz to the room.

Each participant leaves the workshop with a functional OpenType font with key glyphs, and the knowledge about how to take the design all the way to a complete and useful typeface design.

The first workshop was held in Edmonton, Canada, in August 2012, as a 5 day bootcamp for students and professional graphic designers. Kyle Fox and Jeff Archibald, a pair of Edmontonian designers, knew I was in town and proposed a workshop. Two months later they had organized one for 40 people. In the fall Alexei Vanyashin
set up a set of workshops in Ukraine, where there is also a lack of type design education available to local designers. 

So far, workshops have been taught by a number of professional type designers including myself, Alexei Vanyashin, Eben Sorkin, Octavio Pardo and Vernon Adams.
 
[[craftingtype.com]]
