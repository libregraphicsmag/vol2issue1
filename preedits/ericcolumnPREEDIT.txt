A journey through form fields
Eric Schrijver

The screen you get confronted with when you log into a wordpress admin and you choose ‘new entry’, is the base of the online writing experience. Little has changed from the very first content management systems, where a single unstyled textfield would let you pour in your thoughts and send them off. The only notable difference is that the interface now allows you to insert links, and style your text. But this is not ‘What you see is what you get’. There is still little resemblance between the field you fill in here, and the finished post that comes out at the other end.

Over the last decade I have been using many tools web sites have offered me for writing. It can be frustrating to see that the basic writing experience has changed so little over this time.  I would like to share some writing experiences that have changed my perception of online writing.

What you see is what you get

A website that I got to know in 2004 is the Amsterdam magazine/web platform/art organisation Mediamatic, showing off the potential of web typography in a striking all-Georgia layout. Next to showing me that one can design convincingly with the native web technologies, it opens up a whole new editing experience. Editing a page,  typing in the title, it arrives all grand and Italic. That one can get away from the default browser style for a text-area, and edit in the same style as the final presentation, is nothing short of a revelation—even if desktop software has been showing this is possible for quite some time already.

Why then did this pattern not propagate? Mediamatic was not the only site with this kind of approach. Flickr also allowed you to edit photo’s metadata in-place, maintaining the look and feel of the final presentation. Part of the answer might lie in the web standards movement, with the focus on tight control of the underlying html, who’s proponents often went so far as to brand WYSIWYG tools as unprofessional. Next to that, websites are nowadays built around generic blog and content management systems like Wordpress, that add on the design like a user-facing ‘theme’ separating the editing process from the site the final visitor reads. And finally, for any such solution to become popular, it has to be embraced by developers, who work on open source projects, so that there can be implementations that can easily be shared, distributed and reimplemented. 
 Part of the problem is the distrust programmers have for WYSIWYG solutions. Used to handling problems with code, plain text is at the core of programming culture. The success of the Markdown standard, conceived in 2004 as well, illustrates this. This typewriter style markup language has seen a large take up and is now used by most programmer-centric websites for user content creation.
 It remains to be seen, however, if plain text solutions can provide a gratifying user experience outside of the audience of software developers.
 
There are no dead links, just links waiting to be born.

In 2008 I write a lot. In order to structure my writing, I install a DokuWiki. The wiki is supposed to be the content management system that places the least possible restrictions on editing, thus enabling the collaboration style that has made Wikipedia possible. For me there is another mind-changing side effect. In a traditional web system, following a link to a page that doesn’t exist will produce an error message. A wiki will inform you beforehand that a link points to a non-existing page, by colouring this link differently. Following the link, then, does not produce an error message. Instead, it presents you with an empty page, with a title taken from the link address. The software invites you to start editing this page.

For me this helps me offload the cognitive stress of writing. Writing makes me think of other things to write. But I can not think of all the texts I want to write all the time while I am writing. Now with the wiki I can insert links to unwritten texts whenever I think of one. The red-coloured links serve to remind me, and in the case of a shared wiki, the other editors of what’s still to create.

All together now

The traditional web form field is a lonely place. You are typing in your information in the browser, and only when you hit a ‘submit’ button the information is sent to the server. This means if you started editing a resource at the same time as someone else, the person who sends their information latest will override the contributions of the predecessor. Content management systems put in place ‘editing locks’, allowing only one user to edit at a time. Wikipedia employs a sophisticated merging tool to merge various edits together.

When I start working with OSP and Constant in 2010, I meet Etherpad. Etherpad presents you with an online document allowing you to start typing. As you do, you might see others who connect start typing as well. There is no submit button. Everything is saved while you type so that it can be updated to the others at the same time. You are no longer solitary with your textbox.

Once you have used Etherpad to write, it becomes difficult to imagine writing collaboratively without it.
 In a collaborative book sprint in Rotterdam, we use Flossmanual’s booki.cc, which allows for a sophistated PDF and ebook creation. Yet like many content editing tools it poses a single user content lock per chapter. At the end of the session it turns out everybody has used Etherpad to write their chapters together, before submitting it to the booki platform.

So will we see more etherpad style collaboration online? Etherpad was bought by Google. Parts of it have been re-used in Google Docs and Google Wave, Google’s mixup of chat, wiki, blog and mail. The technology behind it has remained notoriously difficult to implement though. It is hard because it requires an intimate collaboration between what is traditionally considered the front- and the backend of the website, the site displaying the data and the system processing and storing it. You need a kind of persistent connection not offered by traditional web servers.

This technology is getting more accessible though. JavaScript, the language once used solely for creating scrolling ticker tape effects on web pages, is making its way into the backend, running in browsers and on servers alike. Frameworks like Meteor JS might make this technology accessible to a large number of developers in the same way that a framework like jQuery has enabled many designers to implement JavaScript effects and interactivity on their sites.

The future is being written

Open source writing tools have been hugely succesful. Wordpress powers 16% of the web. Wikipedia is the go-to resource for factual knowledge. As the intimate link between Wikipedia and its editing software shows, we can’t talk about the writing technologies of the future without talking about the texts of the future.
 Writing tools rise in response to, and at the same time give shape to, what is being written.
 
We immediately recognize the tone of Wikipedia even if an article is written by thousands of people. The tool, and the process it entails, stimulate a certain kind of discourse. At the same time, these tools have responded to needs and desires that where there already—the reason they were invented in the first place. I conveniently skipped this question in what preceded. But before one invents a writing tool, one might need to ask: ‘what do I want to write?’


